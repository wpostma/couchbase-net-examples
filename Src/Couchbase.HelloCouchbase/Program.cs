﻿using System;
using Couchbase.Core;
using Couchbase.Configuration.Client;

namespace Couchbase.HelloCouchbase
{
    class Program
    {
      
        
        static void Main(string[] args)
        {
             
            ClientConfiguration _config = new ClientConfiguration();
            _config.Servers.Add(new Uri("http://couchbase1.ramsoft.biz:8091/pools"));
            //_config.BucketConfigs.Add(...);
            var _cluster = new Cluster(_config);

            using (var bucket = _cluster.OpenBucket() ) 
            {
                var document = new Document<dynamic>
                {
                    Id = "Hello",
                    Content = new
                    {
                        Name = "Couchbase"
                    }
                };

                var upsert = bucket.Upsert(document);
                if (upsert.Success)
                {
                    var get = bucket.GetDocument<dynamic>(document.Id);
                    document = get.Document;
                    var msg = string.Format("{0} {1}!", document.Id, document.Content.Name);
                    Console.WriteLine(msg);

                    Console.WriteLine("couchbase communications success.  press enter to continue.");
                } else {
                    Console.WriteLine("upsert failed. press enter to continue.");
                }
                
                Console.Read();
            }
        }
    }
}
    