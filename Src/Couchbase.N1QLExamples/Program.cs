﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Couchbase.Core;
using Couchbase.Configuration.Client;
using Couchbase.N1QL;
using Couchbase.Utils;


namespace Couchbase.N1QLExamples
{
    class Program
    {

        static void Main(string[] args)
        {
            try
            {
                int Count = 0;
                IQueryResult<dynamic> result;
                Console.WriteLine("Couchbase N1QL example");

                ClientConfiguration _config = new ClientConfiguration();
                _config.Servers.Add(new Uri("http://couchbase1.ramsoft.biz:8091/pools"));
                
                var _cluster = new Cluster(_config);

                using (var bucket = _cluster.OpenBucket("default"))
                {
                    const string query = "SELECT COUNT(*) FROM default";

                    QueryRequest request = new QueryRequest(query);
                    
                    result = bucket.Query<dynamic>(request);
                    foreach (var row in result.Rows)
                    {
                        Console.WriteLine(row);
                        Count++;
                    }
                }

                if (result.Status == Couchbase.N1QL.QueryStatus.Fatal)
                {
                    Console.WriteLine("Query failed.");
                }
                else if (Count == 0)
                {
                    Console.WriteLine("No results returned.");
                }

                if (result.Warnings.Count>0)
                {
                    Console.WriteLine(result.Warnings[0].Message);
                }

                if (result.Errors.Count > 0)
                {
                    Console.WriteLine(result.Errors[0].Message);
                }

                
                _cluster.Dispose();


            }
            catch (System.AggregateException ex)
            {
                Console.WriteLine(ex.InnerException.Message); // One or more errors : Probably No Couchbase 4.0 N1QL Service Active on this Node.
                //throw;
            }

            Console.Write("Press ENTER to Continue...");
            Console.Read();

        }
    }
}
